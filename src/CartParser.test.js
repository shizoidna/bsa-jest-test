import CartParser from './CartParser';

describe('CartParser', () => {
  let parser;

  beforeEach(() => {
    parser = new CartParser();
  });

  describe('constructor', () => {
    it('check object initialized', () => {

      expect(parser.ColumnType).toEqual(expect.any(Object));

      expect(parser.ErrorType).toEqual(expect.any(Object));

      expect(parser.schema).toEqual(expect.any(Object));
    })
  })


  describe('parse', () => {
    it('should parse data by absolute path', () => {
      const path = '/path';

      parser.readFile = jest.fn();
      parser.readFile.mockReturnValueOnce(`
      Product name,Price,Quantity
      Mollis consequat,9.00,2
      Tvoluptatem,10.32,1
      Scelerisque lacinia,18.90,1
      Consectetur adipiscing,28.72,10
      Condimentum aliquet,13.90,1`);

      const result = parser.parse(path);

      expect(parser.readFile).toHaveBeenCalledWith(path);

      expect(result).toMatchObject({
        items: expect.any(Array),
        total: expect.any(Number)
      });
    });


    it('should throw error by invalid data', () => {
      const path = '/path';

      parser.readFile = jest.fn();
      parser.readFile.mockReturnValueOnce(`
      Product name,Price,Quantity
      Mollis consequat,9.00,2
      Tvoluptatem,10.32,1
      Scelerisque lacinia,18.90,1
      Consectetur adipiscing,28.72,10
      Condimentum aliquet,-13.90,-1`);

      expect(() => parser.parse(path)).toThrow('Validation failed!');
    })
  })


  describe('readFile', () => {
    it('should be called module', () => {
      const path = "/path";

      parser.readFile = jest.fn();

      parser.readFile(path);
      expect(parser.readFile).toHaveBeenCalledWith(path);
    })
  })


  describe('validate', () => {
    it('should add error with type header', () => {
      const contents = `
				Product name, Price, FAKE
				Price,,
				Quality,,
			`;

      const result = parser.validate(contents);

      const expected = expect.objectContaining({
        type: 'header',
        message: expect.stringContaining('FAKE'),
        column: expect.any(Number)
      });

      expect(result).toContainEqual(expected);
    });


    it('should add error with type row', () => {
      const contents = `
				Product name,Price,Quantity
        Mollis consequat,9.00,2
        Tvoluptatem,10.32,1
        Scelerisque lacinia,18.90,1
        Consectetur adipiscing,28.72,10
        one,2`;

      const result = parser.validate(contents);

      const expected = expect.objectContaining({
        type: 'row',
        message: expect.stringContaining('2'),
        row: expect.any(Number),
        column: -1
      });

      expect(result).toContainEqual(expected);
    });


    it('should add error with cell connected with empty string', () => {
      const contents = `
				Product name,Price,Quantity
        Mollis consequat,9.00,2
        Tvoluptatem,10.32,1
        Scelerisque lacinia,18.90,1
        Consectetur adipiscing,28.72,10
        ,,`;

      const result = parser.validate(contents);

      const expected = expect.objectContaining({
        type: 'cell',
        message: expect.stringContaining('Expected cell to be a nonempty string but received'),
        row: expect.any(Number)
      });

      expect(result).toContainEqual(expected);
    });


    it('should add error with cell connected with invalid type data', () => {
      const contents = `
				Product name,Price,Quantity
        Mollis consequat,9.00,2
        Tvoluptatem,10.32,1
        Scelerisque lacinia,18.90,1
        Consectetur adipiscing,28.72,10
        Condimentum aliquet,13.90,FAKE`;

      const result = parser.validate(contents);

      const expected = expect.objectContaining({
        type: 'cell',
        message: expect.stringContaining('Expected cell to be a positive number but received "FAKE"'),
        row: expect.any(Number)
      });

      expect(result).toContainEqual(expected);
    });


    it('should have no errors', () => {
      const contents = `
      Product name,Price,Quantity
      Mollis consequat,9.00,2
      Tvoluptatem,10.32,1
      Scelerisque lacinia,18.90,1
      Consectetur adipiscing,28.72,10
      Condimentum aliquet,13.90,1`

      const result = parser.validate(contents);

      expect(result).toHaveLength(0);
    })
  });

  describe('parseLine', () => {
    it('should parse line from csv-format to object', () => {
      const line = 'Mollis consequat,9.00,2';

      const result = parser.parseLine(line);

      expect(result).toMatchObject({
        id: expect.any(String),
        name: 'Mollis consequat',
        price: 9.00,
        quantity: 2
      });
    })
  })

  describe('calcTotal', () => {
    it('calculate price for order', () => {
      const data = [{
        name: 'Mollis consequat',
        price: 9,
        quantity: 2,
        id: '975306f4-0d39-43b8-8b53-b21fe99069ba'
      },
        {
          name: 'Tvoluptatem',
          price: 10.32,
          quantity: 1,
          id: '308d00b4-40d2-4ca7-9b58-4afb6bc39270'
        },
        {
          name: 'Scelerisque lacinia',
          price: 18.9,
          quantity: 1,
          id: 'a484c54a-640e-4f53-8913-c93d99af10e3'
        },
        {
          name: 'Consectetur adipiscing',
          price: 28.72,
          quantity: 10,
          id: '99f53b1f-b534-4b46-9a4f-87d443ea1868'
        },
        {
          name: 'Condimentum aliquet',
          price: 13.9,
          quantity: 1,
          id: '92e94e74-36bc-49ed-a847-48bf2548a193'
        }
      ]

      const result = parser.calcTotal(data);

      expect(result).toBe(348.31999999999994);
    })

  })

});
